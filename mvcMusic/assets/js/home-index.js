﻿
var app = angular.module("appMusic", []);

app.controller('MainController', ['$scope', function ($scope) {
    $scope.title = 'MUSIC ONLINE';
    $scope.musics = [
        {
            id: 1,
            album: "Noite Acústica",
            artist: "nirlvana",
            track: "nirvana",
            track: "nirvana",
            image: "/assets/img/trash/temp1.PNG",
            favorite: "off"
        },
	    {
	        id: 2,
	        album: "slipkaaaaanot",
	        artist: "aaaaaa",
	        track: "slipkaaaaanot",
	        image: "/assets/img/trash/temp1.PNG",
	        favorite: "off"
	    },
	    {
	        id: 3,
	        album: "bbbbbb",
	        artist: "bbbb",
	        track: "slipkaaaaanot",
	        image: "/assets/img/trash/temp1.PNG",
	        favorite: "off"
	    },
	    {
	        id: 4,
	        album: "slipkaaaaanot",
	        artist: "abbcbbbaaaaa",
	        track: "slipkcaaaaanot",
	        image: "/assets/img/trash/temp1.PNG",
	        favorite: "off"
	    },
	    {
	        id: 5,
	        album: "accccccaaaaaa",
	        artist: "aacccaaaa",
	        track: "slipkaaaaanot",
	        image: "/assets/img/trash/temp1.PNG",
	        favorite: "off"
	    }
    ];
    $scope.addFavorite = function (index) {
        if ($scope.musics[index].favorite == "off") {
            $scope.musics[index].favorite = "on";
        }else if ($scope.musics[index].favorite == "on") {
            $scope.musics[index].favorite = "off";
        }
    };

    //$scope.foods = [];

    //$http.get("/api/musics/MusicsController")
    //.then(
    //// success
    //function (r) {
    //    var data = r.data;
    //    $scope.foods = data.results;
    //},
    //// failure
    //function (error) {
    //    alert("Bad things happen to good people");
    //});

}]);