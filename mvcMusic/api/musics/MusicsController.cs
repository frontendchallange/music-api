﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Ubiwhere2.api.musics
{
    [System.Web.Mvc.Route("api/[controller]")]
    public class MusicsController : Controller
    {
        // GET: api/values
        [System.Web.Mvc.HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //// GET api/values/5
        //[HttpGet()]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut()]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete()]
        //public void Delete(int id)
        //{
        //}
    }
}
