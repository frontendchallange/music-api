﻿module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        uglify: {
            my_target: {
                files: { 'assets/js/app.js': ['assets/js/app.js', 'assets/js/**/*.js'] }
            }
        },

        watch: {
            scripts: {
                files: ['assets/final/**/*.js'],
                tasks: ['uglify']
            }
        }
    });

    grunt.registerTask('default', ['uglify', 'watch']);
};