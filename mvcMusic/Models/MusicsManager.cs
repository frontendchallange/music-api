﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ubiwhere2.Models
{
    public class MusicsManager
    {

        readonly List<Music> _musics = new List<Music>() {
            new Music { id = 1, album = "Bristleback", artist="Strength", track="Strength"},
            new Music { id = 2, album ="Abbadon", artist="Strength", track="Strength"},
            new Music { id = 3, album ="Spectre", artist="Agility", track="Strength"},
            new Music { id = 4, album ="Juggernaut", artist="Agility", track="Strength"},
            new Music { id = 5, album ="Lion", artist="Intelligence", track="Strength"},
            new Music { id = 6, album ="Zues", artist="Intelligence", track="Strength"},
            new Music { id = 7, album ="Trent", artist="Strength", track="Strength"},
        };
        public IEnumerable<Music> GetAll { get { return _musics; } }

        public List<Music> GetHeroesByTrack(string name)
        {
            return _musics.Where(o => o.track.ToLower().Equals(name.ToLower())).ToList();
        }

        public Music GetHeroByID(int Id)
        {
            return _musics.Find(o => o.id == Id);
        }
    }
}
