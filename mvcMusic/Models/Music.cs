﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ubiwhere2.Models
{
    public class Music
    {
        public int id { get; set; }
        public string album { get; set; }
        public string artist { get; set; }
        public string track { get; set; }
    }
}
